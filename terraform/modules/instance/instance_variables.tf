variable "name" {
  type = string
  description = "Name of instance"
}
variable "hostname" {
  type = string
  description = "Hostname of instance"
}
variable "image_name" {
  type = string
  description = "Image name for instance"
  default = "ubuntu-1804-lts"
}
variable "subnet_id" {
  type = string
  description="ID resource yandex_vpc_subnet"
}
variable "zone" {
  type = string
  default     = "ru-central1-a"
}