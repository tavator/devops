terraform {
  required_version = ">= 1.3"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.80.0"
    }
  }
}

data "yandex_compute_image" "my_image" {
  family = var.image_name
}

resource "yandex_compute_instance" "default" {

  name = var.name
  hostname = var.hostname
  zone = var.zone

  resources {
    cores         = 2
    memory        = 2
    core_fraction = 100
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.my_image.id
      size = 20
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = "true"
  }

  scheduling_policy {
    preemptible = false
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}
