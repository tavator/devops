output "External_IP" {
    value=resource.yandex_compute_instance.default.network_interface[0].nat_ip_address
    description="External IP instance"
}
output "Internal_IP" {
    value=resource.yandex_compute_instance.default.network_interface[0].ip_address
    description="Internal IP instance"
}