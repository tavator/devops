terraform {
  required_version = ">= 1.3"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.80.0"
    }
  }
}

provider "yandex" {
  service_account_key_file = "service_account.key"
  cloud_id                 = local.cloud_id
  folder_id                = local.folder_id
  zone                     = var.zone
}

resource "yandex_vpc_network" "devops_net" {
  name = "devops_net"
}

resource "yandex_vpc_subnet" "devops_subnet" {
  name           = "devops_subnet"
  zone           = var.zone
  network_id     = yandex_vpc_network.devops_net.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}

module "instance_k8s_master" {
  hostname   = "instance-k8s-master"
  source     = "./modules/instance"
  name       = "instance-k8s-master"
  subnet_id  = resource.yandex_vpc_subnet.devops_subnet.id
}

module "instance_k8s_app" {
   hostname   = "instance-k8s-app"
   source    = "./modules/instance"
   name      = "instance-k8s-app"
   subnet_id = resource.yandex_vpc_subnet.devops_subnet.id
 }

module "instance_srv" {
   hostname   = "instance-srv"
   source    = "./modules/instance"
   name      = "instance-srv"
   subnet_id = resource.yandex_vpc_subnet.devops_subnet.id
 }


