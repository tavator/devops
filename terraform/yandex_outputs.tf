output "master1" {
  value       = module.instance_k8s_master.External_IP
  description = "External IP address for instance k8s_master"
}
output "worker1" {
  value       = module.instance_k8s_app.External_IP
  description = "External IP address for instance k8s_app"
}
output "srv" {
  value       = module.instance_srv.External_IP
  description = "External IP address for instance srv"
}