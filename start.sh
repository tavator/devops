#!/bin/bash
export ANSIBLE_HOST_KEY_CHECKING=False
terraform   -chdir=./terraform init
terraform   -chdir=./terraform apply -auto-approve
ips=`terraform -chdir=./terraform output`
IFS=$'\n'
for ip in $ips
do
case $ip in
  *"master"*)
    echo  "[masters]" > ./ansible/hosts
    echo  -n `echo $ip | cut -f1 -d ' '` >> ./ansible/hosts
    echo  -n " ansible_host=" >> ./ansible/hosts
    echo  -n `echo $ip | cut -f3 -d ' '| tr -d \"` >> ./ansible/hosts
    echo   " ansible_user=ubuntu" >> ./ansible/hosts
    while ! nc -zw 1 `echo $ip | cut -f3 -d ' '| tr -d \"` 22 > /dev/null 2>&1
    do
        echo "Wait master..."
        sleep 1
    done
    echo "Master active"
    ;;
  *"worker"*)
    echo  "[workers]" >> ./ansible/hosts
    echo  -n `echo $ip | cut -f1 -d ' '` >> ./ansible/hosts
    echo  -n " ansible_host=" >> ./ansible/hosts
    echo  -n `echo $ip | cut -f3 -d ' '| tr -d \"` >> ./ansible/hosts
    echo   " ansible_user=ubuntu" >> ./ansible/hosts
    while ! nc -zw 1 `echo $ip | cut -f3 -d ' '| tr -d \"` 22 > /dev/null 2>&1
    do
        echo "Wait worker..."
        sleep 1
    done
    echo "Worker active"
    ;;
  *"srv"*)
    echo  "[servers]" >> ./ansible/hosts
    echo  -n `echo $ip | cut -f1 -d ' '` >> ./ansible/hosts
    echo  -n " ansible_host=" >> ./ansible/hosts
    echo  -n `echo $ip | cut -f3 -d ' '| tr -d \"` >> ./ansible/hosts
    echo   " ansible_user=ubuntu" >> ./ansible/hosts
    while ! nc -zw 1 `echo $ip | cut -f3 -d ' '| tr -d \"` 22 > /dev/null 2>&1
    do
        echo "Wait srv..."
        sleep 1
    done
    echo "srv active"
    ;;
esac
done
echo "[all:vars]" >> ./ansible/hosts
echo "ansible_python_interpreter=/usr/bin/python3" >> ./ansible/hosts
config=`cat config.txt  | grep -Ev "^#"`
for config_str in $config
do
echo $config_str >> ./ansible/hosts
done
# Установка зависимостей 
ansible-playbook -i ./ansible/hosts ./ansible/depend.yaml
# Создание кластера k8s
ansible-playbook -i ./ansible/hosts ./ansible/master.yaml
# Добавление в кластер сервера
ansible-playbook -i ./ansible/hosts ./ansible/workers.yaml
# Настройка сервера мониторинга и gitlab-runner
ansible-playbook -i ./ansible/hosts ./ansible/srv.yaml

